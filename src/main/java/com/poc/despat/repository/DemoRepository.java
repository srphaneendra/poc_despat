package com.poc.despat.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.poc.despat.entities.DemoEntity;


public interface DemoRepository extends JpaRepository<DemoEntity, Integer>{
	
}
