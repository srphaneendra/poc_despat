package com.poc.despat.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.poc.despat.service.ResilienceService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class ResilienceControl {
	
	@Autowired
	ResilienceService resilienceService;
	
// works with spring aop and actuator as dependencies for initial config
	@GetMapping (path = "/circuitBreak")
	public ResponseEntity circuitBreakCheck(@RequestParam String uri) {		
		//log.info("Testing circuitBreaker Controller...");
		return resilienceService.returnRestObject(uri);
	}
	
}
