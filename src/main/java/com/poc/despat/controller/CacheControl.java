package com.poc.despat.controller;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.poc.despat.entities.DemoEntity;
import com.poc.despat.repository.DemoRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class CacheControl {
	
	@Autowired
	DemoRepository demoRepo;
	
	@Autowired
	CacheManager cacheManager;
	
	@Autowired
	RedisTemplate<String, String> redisTemplate;
	
	@Cacheable (value= "demoValue",keyGenerator = "dynamicUniqueKey")
	@GetMapping ("/fetchDBRecord")
	public Optional<DemoEntity> fetchDBRecord (@RequestParam Integer id) {
		log.info("Fetching DB record for "+id);
		return demoRepo.findById(id);
	}
	
	@GetMapping (path = "/clearCache")
	public HttpStatus clearAllCache() {
		try {
			Collection<String> cacheNames = cacheManager.getCacheNames();
			log.info("Clearing cache for {}", cacheNames.toString());
			for (String cache: cacheNames) {
				cacheManager.getCache(cache).clear();
			}
			return HttpStatus.OK;
		}catch (Exception e) {
			log.error("Clear cache failed {}",e.getMessage());
		}
		return HttpStatus.SERVICE_UNAVAILABLE;
	}
	
	@GetMapping ("/fetchRedisData")
	public HttpStatus fetchRedisData() {
		Set<String> keys = redisTemplate.keys("*");
		Iterator<String> it = keys.iterator();
		
		while (it.hasNext()) {
			String key = it.next();
			log.info("Value Fetched {} for key {}", redisTemplate.opsForValue().get(key), key);
			//log.info("Value is {} ",redisTemplate.opsForValue().get("demoValue::1"));
		}
		return HttpStatus.OK;
	}

}
