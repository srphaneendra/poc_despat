package com.poc.despat.entities;

public enum Status {

    CHUNK_COMPLETED,
    COMPLETED,
    FAILED;

}
