package com.poc.despat.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table (name = "billionaires")
public class DemoEntity implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column (name = "ID")
	private int Id;
	
	@Column (name = "FIRST_NAME")
	private String name;
	
}
