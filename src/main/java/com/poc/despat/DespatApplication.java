package com.poc.despat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DespatApplication {

	public static void main(String[] args) {
		SpringApplication.run(DespatApplication.class, args);
	}

}
