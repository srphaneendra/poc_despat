package com.poc.despat.service;

import java.net.UnknownHostException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.UnknownContentTypeException;

import com.poc.despat.customexceptions.NewException;

import io.github.resilience4j.bulkhead.BulkheadFullException;
import io.github.resilience4j.bulkhead.annotation.Bulkhead;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ResilienceService {
	
	@Autowired
	RestTemplate restTemplate;

//We expect this method to fallback to fallingback when there is exception / out of threshold we configure
// name = 'default' is reference to application.properties specific set of values
	
	//@Retry (fallbackMethod = "RetryFallback", name = "default")
	@CircuitBreaker ( fallbackMethod = "CBfallingBack", name = "CBConfig")
	@Bulkhead (fallbackMethod = "BHFallback", name = "default")

	public ResponseEntity returnRestObject(String uri) {
		log.info("Regular execute");
		return restTemplate.getForEntity(uri, Object.class);
	}
	
	
//Resilience4j allows to define any number of exception fallbacks for single method.

	public ResponseEntity<Object> CBfallingBack( String uri, Exception e){
		log.info("CBFB1..");
		return new ResponseEntity("CB Fallback1: Failed due unreachable host."+uri+e.getMessage(), HttpStatus.OK);
	}
	
	public ResponseEntity<Object> CBfallingBack (String uri, IllegalArgumentException e){
		log.info("CBFB2");
		return new ResponseEntity("CB Fallback2: Failed due incorrect url. "+uri+e.getMessage(), HttpStatus.OK);
	}
	
	public ResponseEntity<Object> CBfallingBack (String uri, UnknownContentTypeException e){
		log.info("CBFB3");
		return new ResponseEntity("CB Fallback3: Failed due incorrect response parsing. "+uri+e.getMessage(), HttpStatus.OK);
	}
	
	public ResponseEntity BHFallback (String uri, BulkheadFullException e) {
		log.info("BHFB1..");
		return new ResponseEntity("BH Fallback: Exceeded maxConcurrent calls property.."+uri, HttpStatus.OK);
	}
	
	public ResponseEntity RetryFallBack() {
		log.info("RETFB1..");
		return new ResponseEntity("RetryFallBack: Exceeded retries ..", HttpStatus.OK);
	} 
	
}
