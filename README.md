
## POC_DesignPatterns

Highlevel POC's for new analysis

#### Design Patterns - Request paths
- CircuitBreaker 
    - Success: 
        - [Properly formatted URL](http://localhost:8080/circuitBreak?uri=https://www.gitlab.com)
    - Failure case:
        - [incorrect URI Format](http://localhost:8080/circuitBreak?uri=1-2)
        - [Incorrect domain/ unknownHost](http://localhost:8080/circuitBreak?uri=https://gully.com)
- Redis
    - http://localhost:8080/fetchDBRecord?id=1
    

#### Reference documents
 - https://resilience4j.readme.io/
    - [Circuit Breaker](https://resilience4j.readme.io/docs/circuitbreaker)
    - [Bulk Head](https://resilience4j.readme.io/docs/bulkhead)
    - [Retry Pattern](https://resilience4j.readme.io/docs/retry)
  - [Redis windows](https://github.com/ServiceStack/redis-windows/blob/master/downloads/redis64-2.8.19.zip)
    - redis-server (for PC's low on memory --maxheap 1G) - This starts cache server
    - redis-cli <command>
        - monitor : in parallel during application execution helps dev check if keys are added are read
        - keys * : to check keys being cached
        - get keyName : to check the value for particular key